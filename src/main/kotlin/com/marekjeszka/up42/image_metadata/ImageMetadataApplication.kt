package com.marekjeszka.up42.image_metadata

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ImageMetadataApplication

fun main(args: Array<String>) {
	runApplication<ImageMetadataApplication>(*args)
}
