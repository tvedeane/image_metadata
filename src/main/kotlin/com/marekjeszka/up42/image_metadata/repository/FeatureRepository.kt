package com.marekjeszka.up42.image_metadata.repository

import com.marekjeszka.up42.image_metadata.model.Feature
import java.util.UUID

interface FeatureRepository {
    fun findById(id: UUID): Feature?

    fun getAll(): Collection<Feature>

    fun findImageById(id: UUID): ByteArray?
}
