package com.marekjeszka.up42.image_metadata.repository

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.marekjeszka.up42.image_metadata.model.Feature
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.io.File
import java.util.*

@Component
class JsonFeatureRepository(@Value("\${jsonfile.path}") private val jsonFilePath: String)
    : FeatureRepository {

    private val jsonFeatures: Map<UUID, JsonFeatureCollection> = parseJson()

    private val features: Map<UUID, Feature> = jsonFeatures.mapValues { toFeature(it.value) }

    private val featureImages: Map<UUID, ByteArray?> = jsonFeatures.mapValues {
        jsonFeature -> jsonFeature.value.features[0].properties.quicklook?.let { Base64.getDecoder().decode(it) }}

    private fun parseJson(): Map<UUID, JsonFeatureCollection> {
        val inputStream = File(jsonFilePath).inputStream()
        val mapper = ObjectMapper().registerModule(KotlinModule())

        val readValue = mapper.readValue(inputStream, Array<JsonFeatureCollection>::class.java)
        return readValue.map { it.features[0].properties.id to it}.toMap()
    }

    private fun toFeature(it: JsonFeatureCollection): Feature {
        val properties = it.features[0].properties
        val acquisition = properties.acquisition
        return Feature(
                properties.id,
                properties.timestamp,
                acquisition.beginViewingDate,
                acquisition.endViewingDate,
                acquisition.missionName)
    }

    override fun findById(id: UUID): Feature? = features[id]

    override fun getAll(): Collection<Feature> = features.values

    override fun findImageById(id: UUID): ByteArray? = featureImages[id]
}

@JsonIgnoreProperties(ignoreUnknown = true)
data class JsonFeatureCollection(val features: List<JsonFeature>)

@JsonIgnoreProperties(ignoreUnknown = true)
data class JsonFeature(val properties: JsonFeatureProperties)

@JsonIgnoreProperties(ignoreUnknown = true)
data class JsonFeatureProperties(
        val id: UUID, val timestamp: Long, val acquisition: JsonAcquisition, val quicklook: String?)

@JsonIgnoreProperties(ignoreUnknown = true)
data class JsonAcquisition(val beginViewingDate: Long, val endViewingDate: Long, val missionName: String)
