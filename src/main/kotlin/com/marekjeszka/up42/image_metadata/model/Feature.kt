package com.marekjeszka.up42.image_metadata.model

import java.util.*

data class Feature(
        val id: UUID,
        val timestamp: Long,
        val beginViewingTime: Long,
        val endViewingTime: Long,
        val missionName: String)