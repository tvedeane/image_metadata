package com.marekjeszka.up42.image_metadata.controller

import com.marekjeszka.up42.image_metadata.model.Feature
import com.marekjeszka.up42.image_metadata.repository.FeatureRepository
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/features/")
class FeatureController(private val featureRepository: FeatureRepository) {

    @GetMapping
    fun getAllFeatures(): Collection<Feature> {
        return featureRepository.getAll()
    }

    @GetMapping("{id}")
    fun getFeatureById(@PathVariable(value = "id") featureId: UUID): ResponseEntity<Feature> {
        val featureOpt = featureRepository.findById(featureId)

        return featureOpt?.let { ResponseEntity.ok(it) } ?: ResponseEntity.notFound().build()
    }

    @GetMapping("{id}/quicklook",
            produces = [MediaType.IMAGE_PNG_VALUE])
    fun getImageByFeatureId(@PathVariable(value = "id") featureId: UUID):
            ResponseEntity<ByteArray> {
        val imageOpt = featureRepository.findImageById(featureId)

        return imageOpt?.let { ResponseEntity.ok(it) } ?: ResponseEntity.notFound().build()
    }
}