package com.marekjeszka.up42.image_metadata.controller

import com.marekjeszka.up42.image_metadata.model.Feature
import com.marekjeszka.up42.image_metadata.repository.FeatureRepository
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.verifyNoMoreInteractions
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import java.util.*

class FeatureControllerTest() {

    @Mock
    lateinit var featureRepository: FeatureRepository

    @InjectMocks
    lateinit var featureController: FeatureController

    private lateinit var mockMvc: MockMvc

    @BeforeEach
    internal fun setUp() {
        MockitoAnnotations.initMocks(this)
        mockMvc = MockMvcBuilders.standaloneSetup(featureController).build()
    }

    @Test
    fun `returns feature by ID`() {
        val id = UUID.fromString("39c2f29e-c0f8-4a39-a98b-deed547d6aea")
        val expected = Feature(id, 1L, 2L,  3L, "mission")
        given(featureRepository.findById(id)).willReturn(expected)

        mockMvc.perform(get("/features/$id"))
               .andExpect(status().isOk)
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(jsonPath("$.id").value(id.toString()))
               .andExpect(jsonPath("$.timestamp").value(expected.timestamp))
               .andExpect(jsonPath("$.beginViewingTime").value(expected.beginViewingTime))
               .andExpect(jsonPath("$.endViewingTime").value(expected.endViewingTime))
               .andExpect(jsonPath("$.missionName").value(expected.missionName))

        verify(featureRepository).findById(id)
        verifyNoMoreInteractions(featureRepository)
    }

    @Test
    fun `handles missing feature returning 404`() {
        val id = UUID.fromString("39c2f29e-c0f8-4a39-a98b-deed547d6aea")
        mockMvc.perform(get("/features/$id"))
                .andExpect(status().isNotFound)

        verify(featureRepository).findById(id)
        verifyNoMoreInteractions(featureRepository)
    }

    @Test
    fun `returns all features`() {
        val id1 = UUID.fromString("39c2f29e-c0f8-4a39-a98b-deed547d6aea")
        val expected1 = Feature(id1, 1L, 2L,  3L, "mission")
        val id2 = UUID.fromString("cf5dbe37-ab95-4af1-97ad-2637aec4ddf0")
        val expected2 = expected1.copy(id = id2)
        given(featureRepository.getAll()).willReturn(listOf(expected1, expected2))

        mockMvc.perform(get("/features/"))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$[0].id").value(id1.toString()))
                .andExpect(jsonPath("$[1].id").value(id2.toString()))

        verify(featureRepository).getAll()
        verifyNoMoreInteractions(featureRepository)
    }

    @Test
    fun `handle no features returning empty body`() {
        given(featureRepository.getAll()).willReturn(emptyList())

        mockMvc.perform(get("/features/"))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(jsonPath("$[0]").doesNotExist())

        verify(featureRepository).getAll()
        verifyNoMoreInteractions(featureRepository)
    }

    @Test
    fun `returns image by feature ID`() {
        val id = UUID.fromString("39c2f29e-c0f8-4a39-a98b-deed547d6aea")
        val expected = Base64.getDecoder().decode("123456")
        given(featureRepository.findImageById(id)).willReturn(expected)

        mockMvc.perform(get("/features/$id/quicklook"))
                .andExpect(status().isOk)
                .andExpect(content().contentType(MediaType.IMAGE_PNG_VALUE))
                .andExpect(content().bytes(expected))

        verify(featureRepository).findImageById(id)
        verifyNoMoreInteractions(featureRepository)
    }

    @Test
    fun `handles missing image returning 404`() {
        val id = UUID.fromString("39c2f29e-c0f8-4a39-a98b-deed547d6aea")
        mockMvc.perform(get("/features/$id/quicklook"))
                .andExpect(status().isNotFound)

        verify(featureRepository).findImageById(id)
        verifyNoMoreInteractions(featureRepository)
    }
}