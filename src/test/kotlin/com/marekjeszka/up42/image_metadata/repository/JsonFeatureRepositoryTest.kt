package com.marekjeszka.up42.image_metadata.repository

import com.marekjeszka.up42.image_metadata.model.Feature
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.util.*

internal class JsonFeatureRepositoryTest {

    private val underTest = JsonFeatureRepository("src/test/resources/source-data.json")

    @Test
    fun `finds feature by ID`() {
        val actual = underTest.findById(UUID.fromString("39c2f29e-c0f8-4a39-a98b-deed547d6aea"))
        val expected = Feature(
                UUID.fromString("39c2f29e-c0f8-4a39-a98b-deed547d6aea"),
                1554831167697L,
                1554831167697L,
                1554831202043L,
                "Sentinel-1B")

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `gets all features`() {
        val actual = underTest.getAll()
        val expected = listOf(
                Feature(UUID.fromString("39c2f29e-c0f8-4a39-a98b-deed547d6aea"),
                        1554831167697L,
                        1554831167697L,
                        1554831202043L,
                        "Sentinel-1B"),
                Feature(UUID.fromString("cf5dbe37-ab95-4af1-97ad-2637aec4ddf0"),
                        1556904743783L,
                        1556904743783L,
                        1556904768781L,
                        "Sentinel-1B"))

        assertThat(actual).containsAll(expected)
    }

    @Test
    fun `finds image by ID`() {
        val actual = underTest.findImageById(UUID.fromString("39c2f29e-c0f8-4a39-a98b-deed547d6aea"))

        assertThat(actual).isNotEmpty() // not sure how to check this long String for now
    }
}