# Image metadata backend

Simple backend application that is capable of working with image metadata.

## Usage

Application exposes following endpoints:
- GET `/features/` returns list of features
- GET `/features/{id}` returns single representation of a feature
- GET `/features/{id}/quicklook` returns the image for the given ID

## Development

### Requirements

Java (minimum 1.8.x)

### Running tests

`./mvnw test`

### Running the app

`./mvnw spring-boot:run`

Application will start running at <http://localhost:8080>.

### Architecture decisions

- architectural model is a standard 3 layer model (controller, service, repository), 
  however currently there is no service logic, so controller directly uses repository
- service layer could be added when the application will be extended with some business logic
- application is open for extension regarding where the data is coming from — 
  adding new repository (e.g. data from a database) 
  would require implementing a new version of `FeatureRepository` interface
- current implementation of `JsonFeatureRepository` is sacrificing application's startup time
  for faster REST API responses (using locally two maps — one for features, one for images)

### Next steps

- add Docker file
- describe API in OpenAPI YAML file
- add pagination to `/features/` endpoint